#!/usr/bin/python

# imported modules - note - python 2.7
import subprocess
import datetime
import sys
import smtplib
from email.mime.text import MIMEText
from datetime import date
datetime.date.today().strftime("%d")

# setup
mailfile = "/home/users/foo/mailfile"
pwaging = 168
user = "<user we are checking>"
# Adaptable to an array (sorry, list...ugh)
loginchecker = "/usr/bin/logins -xo -l " + user + " | awk -F: '{print $9}'"
command = [ loginchecker ]
now = datetime.datetime.now()
x = now.year
y = now.day
z = now.month

# open our mail filehandle

fh=open(mailfile,"w")

# define a sub and get back the absolute value of our delta.  Just in case
# something goes wonky with the system lib return output; we should get a weird
# value to let us know something is wrong via the mailed output

def diff_dates(date1, date2):
    return abs(date2-date1).days

# perhaps there is a better way to do this rather than defining a command as
# a string and then assigning the string to the 'command' var and THEN using
# that in the Popen call...seriously?  

cmd = subprocess.Popen(command, shell=True,  stdout=subprocess.PIPE)

# process and slice up some data
for line in cmd.stdout:
        line = line.rstrip()

a, b, c = [line[i:i+2] for i in range(0, len(line), 2)]
c = "20" + c

# funny how python uses 40+ year-old typecasting. 
a = int(a)
b = int(b)
c = int(c)

# convert int to str
date1 = date(c,a,b)
date2 = date(x,z,y)

# finally ask the question about dates and get an answer about impending doom
result1 = diff_dates(date2, date1)

# tell the user some useful stuff in a log file (redirect that junk along with stderr)
print >> fh, "Password last set on", date1 , "for", user
print >> fh, "Today is", date2
print >> fh, "Our default aging in days is", pwaging

# do some formatting and dump into the filehandle
if (result1 >= 1):
        daystolockout = abs(pwaging - result1)
        print >> fh, "Password for ", user , " EXPIRES IN ", daystolockout, "DAYS!!!"
elif (result1 == 1):
        print >> fh, "Password for ", user, " is ", result1 , "day old"
else:
        print >> fh, "Password for ", user , "expiration not imminent"
fh.close()


# handle and send some early AM annoyance to the sysadmin...


fm = open(mailfile, 'rb')
msg = MIMEText(fm.read()) # need to MIME this or else....
fm.close()
if (result1 == 154):
        daystolockout = abs(pwaging - result1)
        str_daystolockout = str(daystolockout)
        critical = "Password for " + user + " EXPIRES IN 14 DAYS!!!"
        msg['subject'] = critical
elif (result1 > 154):
        daystolockout = abs(pwaging - result1)
        str_daystolockout = str(daystolockout)
        supercritical = "Password for " + user + " EXPIRES REALLY REALLY SOON!! - in " + str_daystolockout
        msg['subject'] = supercritical
elif (result1 < 154):
        daystolockout = abs(pwaging - result1)
        str_daystolockout = str(daystolockout)
        info = "Snooze: Password for " + user + " expires in " + str_daystolockout + "day(s)"
        msg['subject'] = info

msg['From'] = "system@foo.com"
msg['To'] = "admin@foo.com"
s = smtplib.SMTP('smtp.foo.com')
s.sendmail("admin@system.foo.com", ["admin@foo.com"], msg.as_string())
s.quit()